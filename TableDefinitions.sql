CREATE TABLE IF NOT EXISTS USERS(
    UserID   INT       NOT NULL AUTO_INCREMENT,
    Name     CHAR(32)  NOT NULL,
    Surname  CHAR(32)  NOT NULL,
    Email    CHAR(255) NOT NULL UNIQUE,
    Username CHAR(16)  NOT NULL UNIQUE,
    Password CHAR(60)  NOT NULL,
    
    PRIMARY KEY (UserID),
	
    CONSTRAINT Name_Alphabetical_USERS    CHECK (Name REGEXP '^[a-zA-Z]+$'),        -- alphabetical chars
    CONSTRAINT Surname_Alphabetical_USERS CHECK (Surname REGEXP '^[a-zA-Z]+$'),     -- alphabetical chars
    CONSTRAINT Username_Constraint_USERS  CHECK (Username REGEXP '^[a-zA-Z0-9_]+$') -- alphanumeric and '_'
);

CREATE TABLE IF NOT EXISTS COMPANIES(
    CompanyID    INT       NOT NULL AUTO_INCREMENT,
    Name         CHAR(32)  NOT NULL UNIQUE,
    Email        CHAR(255) NOT NULL UNIQUE,
    PhoneNumber  INT       NOT NULL UNIQUE,
    Logo         BLOB                     ,
    Username     CHAR(16)  NOT NULL UNIQUE,
    Password     CHAR(60)  NOT NULL,
    
    PRIMARY KEY (CompanyID),
	
    CONSTRAINT Name_Alphabetical_COMPANIES    CHECK (Name REGEXP '^[a-zA-Z ]+$'),                -- alphabetical chars and space
    CONSTRAINT Phone_Number_CY_COMPANIES      CHECK (PhoneNumber BETWEEN 10000000 AND 99999999), -- 8 digit number
    CONSTRAINT Username_Constraint_COMPANIES  CHECK (Username REGEXP '^[a-zA-Z0-9_]+$')          -- alphanumeric and '_'
);

CREATE TABLE IF NOT EXISTS SHOPS(
    ShopID       INT             NOT NULL AUTO_INCREMENT,
    Name         CHAR(32)        NOT NULL,
    Address      CHAR(255)       NOT NULL,
    Latitude     DECIMAL(9, 7)   NOT NULL,
    Longitude    DECIMAL(9, 7)   NOT NULL,
    City         CHAR(32)        NOT NULL,
    PostalCode   SMALLINT        NOT NULL,
    PhoneNumber  INT             NOT NULL,
    Username     CHAR(16)        NOT NULL UNIQUE,
    Password     CHAR(60)        NOT NULL,
    CompanyID    INT             NOT NULL,
    
    PRIMARY KEY (ShopID),
    FOREIGN KEY (CompanyID) REFERENCES COMPANIES(CompanyID),
	
    CONSTRAINT Name_Alphabetical_SHOPS    CHECK (Name REGEXP '^[a-zA-Z ]+$'),                -- alphabetical chars and space
    CONSTRAINT LAT_SHOPS                  CHECK (Latitude BETWEEN -90 AND 90),               -- latitude in  [-90,90]
    CONSTRAINT LON_SHOPS                  CHECK (Longitude BETWEEN -180 AND 180),            -- longitude in [-180,180]
    CONSTRAINT Postal_Digits_SHOPS        CHECK (PostalCode BETWEEN 1000 AND 9999),          -- postal code 4 digits
    CONSTRAINT Phone_Number_CY_SHOPS      CHECK (PhoneNumber BETWEEN 10000000 AND 99999999), -- 8 digit number
    CONSTRAINT Username_Constraint_SHOPS  CHECK (Username REGEXP '^[a-zA-Z0-9_]+$')          -- alphanumeric and '_'
);

CREATE TABLE IF NOT EXISTS CATEGORIES(
    CategoryID  INT        NOT NULL AUTO_INCREMENT,
    Category    CHAR(32)   NOT NULL,
    
    PRIMARY KEY(CategoryID)
);

CREATE TABLE IF NOT EXISTS ITEMS(
    ItemID      INT           NOT NULL AUTO_INCREMENT,
    Description CHAR(255)     NOT NULL,
    Brand       CHAR(32)      NOT NULL,
    Category    INT           NOT NULL,
    Price       DECIMAL(8, 2) NOT NULL,
    CompanyID   INT           NOT NULL DEFAULT 1,

    PRIMARY KEY(ItemID),
    FOREIGN KEY(Category)  REFERENCES CATEGORIES(CategoryID),
    FOREIGN KEY(CompanyID) REFERENCES COMPANIES(CompanyID),
    
    CONSTRAINT Price_GTZ CHECK (Price >= 0) -- non negative price
);

CREATE TABLE IF NOT EXISTS SELLS(
    ShopID      INT       NOT NULL,
    ItemID      INT       NOT NULL,
    Quantity    SMALLINT  NOT NULL,
    
    FOREIGN KEY(ShopID) REFERENCES SHOPS(ShopID),
    FOREIGN KEY(ItemID) REFERENCES ITEMS(ItemID),
    
    CONSTRAINT Quantity_GTZ_SELLS CHECK (Quantity >= 0) -- non negative quantity
);