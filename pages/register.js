import { useState } from 'react';
import { faUser, faEnvelope, faKey } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { signIn } from 'next-auth/client';

const Register = () => {
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmationPassword, setConfirmationPassword] = useState('');

  const doPasswordsMatch = password === confirmationPassword;

  const handleSubmit = async () => {
    await fetch('/api/users', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username, name, surname, email, password })
    });

    await signIn('user', { username, password, callbackUrl: '/user' });
  };

  return (
    <div className="hero is-primary is-fullheight">
      <div className="hero-body">
        <div className="container">
          <div className="columns is-centered">
            <div className="column is-4">
              <h3 className="title has-text-white">Register User Account</h3>
              <hr className="login-hr"/>

              <div className="box">
                <div className="field">
                  <label className="label" htmlFor='username'>Username</label>
                  <div className="control has-icons-left">
                    <input className="input" placeholder="Christoforos95" onChange={ e => setUsername(e.target.value) } name='username' value={username} />
                    <span className="icon is-small is-left">
                      <FontAwesomeIcon icon={faUser} />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label" htmlFor='name'>Name</label>
                  <input className="input" placeholder="Christoforos" name='name' onChange={e => setName(e.target.value)} value={name} />
                </div>
                <div className="field">
                  <label className="label" htmlFor='surname'>Surname</label>
                  <input className="input" placeholder="Panayiotou" name='surname' onChange={e => setSurname(e.target.value)} value={surname} />
                </div>
                <div className="field">
                  <label className="label" htmlFor='email'>Email</label>
                  <div className="control has-icons-left">
                    <input className="input" placeholder='cs95gp1@cs.ucy.ac.cy' name="email" onChange={e => setEmail(e.target.value)} value={email} />
                    <span className="icon is-small is-left">
                      <FontAwesomeIcon icon={faEnvelope} />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label" htmlFor='password'>Password</label>
                  <div className="control has-icons-left">
                    <input className="input" placeholder="********" type='password' name='password' onChange={e => setPassword(e.target.value)} value={password} />
                    <span className="icon is-small is-left">
                      <FontAwesomeIcon icon={faKey} />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <label className="label" htmlFor='confirmation-password'>Confirmation Password</label>
                  <div className="control has-icons-left">
                    <input className={"input " + (!doPasswordsMatch ? "is-danger" : '') } placeholder="********" type='password' name='confirmation-password' onChange={e => setConfirmationPassword(e.target.value)} value={confirmationPassword} />
                    <span className="icon is-small is-left">
                      <FontAwesomeIcon icon={faKey} />
                    </span>
                  </div>
                  { !doPasswordsMatch && <p className="help is-danger">Passwords don't match</p> }
                </div>

                <button disabled={!doPasswordsMatch} className="button is-primary is-fullwidth" onClick={handleSubmit}>Submit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Register;
