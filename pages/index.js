import { useSession, signOut } from "next-auth/client";
import { useRouter } from "next/router";

export default function Home() {
  const [session, loading] = useSession()
  const router = useRouter();

  const curUserType = session?.user?.userType || 'user';

  if (!loading && 'user' !== curUserType) {
    router.replace('/user');
  }

  if(loading || 'user' !== curUserType) return null;

  return (
    <div>
      <nav className="navbar has-shadow"  role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item has-text-weight-bold" href="/">
              List'em All
            </a>
          </div>

          <div id="bulma-navbar-menu" className="navbar-menu">
            <div className="navbar-end">
              <div className="navbar-item">
                <div className="buttons">
                  { !session ?
                    <>
                      <a href="/register" className="button is-text">Register</a>
                      <a href="/api/auth/signin" className="button is-primary">
                        <strong>Log in</strong>
                      </a>
                    </> :
                    <>
                      <a onClick={signOut} className="button is-text">Log out</a>
                      <a href="/user" className="button is-primary">
                        <strong>Try Now</strong>
                      </a>
                    </>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <section className="hero is-primary is-bold">
        <div className="hero-body splash">
          <div className="container">
            <div className="columns is-vcentered">
              <div className="column">
                <h1 className="title is-spaced is-size-2-desktop is-size-1-fullhd">Find your favourite supermarkets near you.</h1>

                <h2 className="subtitle">
                  An online web application for finding any products in supermarkets near you.
                </h2>

                <div className="buttons">
                  {!session && <a className="control button is-primary is-inverted is-medium" href="/register">Register →</a>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="section" id="how-it-works">
        <div className="container has-text-centered">
          <div className="section-header">
            <h2 className="title">The first of its kind in Cyprus.</h2>
            <h3 className="subtitle">Shopping Redefined.</h3>
          </div>

          <div className="columns step is-vcentered">
            <div className="column is-6">
              <span className="is-size-1">01</span>
              <h3 className="title is-size-4-mobile">Choose your favourite companies</h3>
              <p>Select the companies from which you prefer to shop.</p>
            </div>

            <div className="column is-6">
              <div className="how-it-works-preview box-shadow has-white-bg">
                <img src="/images/company_selection_icon.jpg" alt="Company Selection Interface" width="300" height="500"/>
              </div>
            </div>
          </div>

          <div className="columns step is-vcentered">
            <div className="column is-6">
              <span className="is-size-1">02</span>
              <h3 className="title is-size-4-mobile">Select supermarkets near you</h3>
              <p>We show you supermarkets close to you from the companies you selected, now you can narrow your selection further.</p>
            </div>
            <div className="column is-6">
              <div className="how-it-works-preview box-shadow has-white-bg">
                <img src="/images/supermarket_selection_icon.jpg" alt="Supermarket Selection Interface" width="400" height="400"/>
              </div>
            </div>
          </div>

          <div className="columns step is-vcentered">
            <div className="column is-6">
              <span className="is-size-1">03</span>
              <h3 className="title is-size-4-mobile">Search products and create shopping lists</h3>
              <p>List'em All provides you with products from the supermarkets you previously chose, all that is left to do is create your shopping list.</p>
            </div>
            <div className="column is-6">
              <div className="how-it-works-preview box-shadow has-white-bg">
                <img src="/images/product_selection_icon.jpg" alt="Product Selection Interface" width="500" height="400"/>
				<img src="/images/list_icon.jpg" alt="Product Selection Interface" width="500" height="400"/>
              </div>
            </div>
          </div>

          <div className="try-demo">
            {!session && <a className="button is-primary is-medium" href="/register">Try Now</a>}
          </div>
        </div>
      </section>
    </div>
  )
}
