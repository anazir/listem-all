import { sqlQuery } from '../../lib/db';

// Returns all users
export default async function handler(req, res) {
  switch(req.method) {
    case 'GET':
      const results = await sqlQuery(`
        SELECT Name, Surname, Username, Email
        FROM USERS
      `);

      res.status(200).json({ users: results });
      break;
    case 'POST':
      // Hash password
      await sqlQuery(`
        INSERT INTO USERS(Name, Surname, Username, Email, Password)
        VALUES(?, ?, ?, ?, ?)
      `, [req.body.name, req.body.surname, req.body.username, req.body.email, req.body.password]);

      res.status(200).json({ success: true });
      break;
  }
}
