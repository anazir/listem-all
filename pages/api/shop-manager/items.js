import { sqlQuery } from '../../../lib/db';
import { getSession } from "next-auth/client";


export default async function handler(req, res) {

    const session = await getSession({req});

    // Check if requester is logged in and authorized
    if(!session || session.user.userType != "shop")
        res.status(401).json({err: "Not Authorized"});
    
    // Get requester's ShopID
    const shopID = session.user.shopID;
    const companyID = session.user.companyID;

    // GET: Returns all items of this shop
    if(req.method == "GET")
    {

        let filter = req.body.category;

        let results;

        // No category filter
        if(!filter)
        {
            results = await sqlQuery(`
            SELECT I.ItemID, I.Description, I.Brand, I.Category, I.Price, S.Quantity
			FROM (SELECT I.ItemID, I.Description, I.Brand, C.Category, I.Price 
			FROM ITEMS I, CATEGORIES C 
			WHERE I.Category = C.CategoryID AND I.CompanyID = ?) AS I
			INNER JOIN
			(SELECT ItemID, Quantity
			FROM SELLS
			WHERE ShopID = ?) AS S
			ON I.ItemID = S.ItemID
			ORDER BY I.ItemID;
            `, [companyID, shopID, shopID]);
        }

        //  Filter by category
        else
        {
			filter = "%" + filter.trim().toLowerCase() + "%";
            results = await sqlQuery(`
            SELECT I.ItemID, I.Description, I.Brand, I.Category, I.Price, S.Quantity
			FROM (SELECT I.ItemID, I.Description, I.Brand, C.Category, I.Price 
			FROM ITEMS I, CATEGORIES C 
			WHERE LOWER(C.Category) LIKE ? AND I.Category = C.CategoryID AND I.CompanyID = ?) AS I
			INNER JOIN
			(SELECT ItemID, Quantity
			FROM SELLS
			WHERE ShopID = ?) AS S
			ON I.ItemID = S.ItemID
			ORDER BY I.ItemID;
            `, [filter,companyID, shopID, shopID]);
        }

        res.status(200).json({success: true,  items: results});
    }

     // PUT: Update stock of an item of this shop
     else if(req.method == "PUT")
     {
         // Check if ItemID exists
         if(!req.body.itemID || (!req.body.stock && req.body.stock != 0))
            res.status(400).json({err: "Parameter is null"});
        else
        {
            // Query
            const result = await sqlQuery(`
            UPDATE SELLS
            SET 
            Quantity = COALESCE(?, Quantity)
            WHERE ItemID = ? AND ShopID = ?;
            `, [req.body.stock, req.body.itemID, req.body.shopID]);

            res.status(200).json({success: true});
        }
     }
}

async function getShopID(username)
{
    let temp = await sqlQuery(`
    SELECT ShopID
    FROM SHOPS
    WHERE Username = ?
    `, [username]);
    if(!temp || Object.keys(temp).length <= 0)
        return null;
    
    return temp[0].ShopID;
}