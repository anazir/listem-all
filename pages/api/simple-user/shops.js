import { sqlQuery } from '../../../lib/db';
import { getSession } from "next-auth/client";

export default async function handler(req, res) {
	const session = await getSession({req});

    // Check if requester is logged in and authorized
    console.log(JSON.stringify(session));
    if(!session || session.user.userType != "user")
        res.status(401).json({err: "Not Authorized"});
	
    // GET: Returns all shops
    if(req.method == "GET") {
		let results = await sqlQuery(`
			SELECT ShopID, Name, Address, Latitude, Longitude, City, PostalCode, PhoneNumber, CompanyID
			FROM SHOPS
			ORDER BY ShopID
		`)
        res.status(200).json({success: true,  shops: results});
    }
}
