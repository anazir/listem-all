import { sqlQuery } from '../../../lib/db';
import { getSession } from "next-auth/client";

export default async function handler(req, res) {
	const session = await getSession({req});

    // Check if requester is logged in and authorized
    console.log(JSON.stringify(session));
    if(!session || session.user.userType != "user")
        res.status(401).json({err: "Not Authorized"});
    
	const shops = req.query.shops.split(','); // array of selected shops (IDs)

    // GET: Returns all items searched by all selected shops
    if(req.method == "GET")
    {
        let filter = req.query.category;
		
		filter = "%" + filter.trim().toLowerCase() + "%";
		let results = [];
		
		for(let shopID in shops) {
			// front end must check if quantity = 0 then item is out of stock
			results.push({"shopID":shops[shopID], "products":await sqlQuery(`
			SELECT I.Description, I.Brand, I.Category, I.Price, S.Quantity
			FROM (SELECT I.ItemID, I.Description, I.Brand, C.Category, I.Price 
			FROM ITEMS I, CATEGORIES C 
			WHERE LOWER(C.Category) LIKE ? AND I.Category = C.CategoryID AND I.CompanyID IN (
				SELECT CompanyID
				FROM SHOPS
				WHERE ShopID = ?
			)) AS I
			INNER JOIN
			(SELECT ItemID, Quantity
			FROM SELLS
			WHERE ShopID = ?) AS S
			ON I.ItemID = S.ItemID
			ORDER BY I.ItemID;
			`, [filter, shops[shopID], shops[shopID]])
			})
		}
        res.status(200).json({success: true,  items: results});
    }
}
