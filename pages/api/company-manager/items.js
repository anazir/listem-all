import { sqlQuery } from '../../../lib/db';
import { getSession } from "next-auth/client";


export default async function handler(req, res) {

    const session = await getSession({req});

    // Check if requester is logged in and authorized
    if(!session || session.user.userType != "company")
        res.status(401).json({err: "Not Authorized"});

    // Get requester's CompanyID
    const companyID = session.user.companyID;
  
    // GET: Returns all items of this company
    if(req.method == "GET")
     {
         // Query
        const results = await sqlQuery(`
        SELECT I.ItemID, I.Description, I.Brand, C.Category, I.Price
        FROM ITEMS I, CATEGORIES C
        WHERE I.CompanyID = ? AND I.Category = C.CategoryID;
        `, [companyID]);

        res.status(200).json({success: true,  items: results });
     }

     // POST: Create new item that belongs to this company
     else if(req.method == "POST")
     {
         // Check if all values are not null/undifined
        if(!req.body.description || !req.body.brand || !req.body.category || !req.body.price)
            res.status(400).json({err: "Parameter is null"});
        else
        {
            // Get category ID of category
            let categoryID = undefined;
            if(typeof(req.body.category) == 'string')
            {
                categoryID = await getCategory(req.body.category);
            }
            else if(typeof(req.body.category) == 'int')
            {
               categoryID = req.body.category;
            }

            // Query
            const results = await sqlQuery(`
            INSERT INTO ITEMS(Description, Brand, Category, Price, CompanyID)
            VALUES (?, ?, ?, ?, ?);
            `, [req.body.description, req.body.brand, categoryID ? categoryID[0].CategoryID: null, req.body.price, companyID]);

            res.status(200).json({success: true});
        }
     }

     // PUT: Update item of this company
     else if(req.method == "PUT")
     {
         // Check if ItemID exists
         if(!req.body.itemID)
            res.status(400).json({err: "Parameter is null"});
        else
        {
            // Get category ID of category
            let categoryID = undefined;
            if(typeof(req.body.category) == 'string')
            {
                categoryID = await getCategory(req.body.category);
            }
            else if(typeof(req.body.category) == 'int')
            {
               categoryID = req.body.category;
            }

            // Query
            const result = await sqlQuery(`
            UPDATE ITEMS
            SET 
            Description =  COALESCE(?, Description),
            Brand = COALESCE(?, Brand),
            Category = COALESCE(?, Category),
            Price = COALESCE(?, Price)
            WHERE ItemID = ? AND CompanyID = ?;
            `, [req.body.description, req.body.brand, categoryID ? categoryID[0].CategoryID: null, req.body.price, req.body.itemID, companyID]);

            res.status(200).json({success: true});
        }
     }

     // DELETE: Delete an item of this company
     else if(req.method == "DELETE")
     {
        // Check if ItemID exists
        if(!req.body.itemID)
        res.status(400).json({err: "Parameter is null"});

        // Query
        await sqlQuery(`
        DELETE FROM SELLS
        WHERE ItemID = ?;
        `, [req.body.itemID]);

        // Query
        const result = await sqlQuery(`
        DELETE FROM ITEMS
        WHERE ItemID = ? AND CompanyID = ?;
        `, [req.body.itemID, companyID]);

        res.status(200).json({success: true});;
     }
     else
    {   
        res.status(500).json({err: "Unknown Request"});
    }
}


async function getCategory(categoryString)
{
    // Get ID of the category
    let categoryID = await sqlQuery(`
    SELECT CategoryID
    FROM CATEGORIES
    WHERE Category = ?
    `, [categoryString]);


    // If the category doesn't exits create it
    if(!categoryID || Object.keys(categoryID).length <= 0)
    {   
        await sqlQuery(`
        INSERT INTO CATEGORIES(Category)
        VALUES(?);
        `, [categoryString]);

        // Get ID of category
        categoryID = await sqlQuery(`
        SELECT CategoryID
        FROM CATEGORIES
        WHERE Category = ?
        `, [categoryString]);
    }


    return categoryID;
}

async function getCompanyID(username)
{
    let temp = await sqlQuery(`
    SELECT CompanyID
    FROM COMPANIES
    WHERE Username = ?
    `, [username]);

    if(!temp || Object.keys(temp).length <= 0)
        return null;
    
    return temp[0].CompanyID;
}
