import { sqlQuery } from '../../../lib/db';
import { getSession } from "next-auth/client";


export default async function handler(req, res) {

    const session = await getSession({req});

    // Check if requester is logged in and authorized
    if(!session || session.user.userType != "company")
        res.status(401).json({err: "Not Authorized"});

    // Get requester's CompanyID
    const companyID = session.user.companyID;

    // GET: Returns all shops of this company
    if(req.method == "GET")
     {
         // Query
        const results = await sqlQuery(`
        SELECT ShopID, Name, Address, City, PostalCode, PhoneNumber, Username
        FROM SHOPS
        WHERE CompanyID = ?;
        `, [companyID]);

        res.status(200).json({success: true, shops: results });
     }

     // POST: Create new shop that belongs to this company
     else if(req.method == "POST")
     {
        // Check if all values are not null/undifined
        if(!req.body.name || !req.body.address || !req.body.city || !req.body.postalCode || !req.body.phoneNumber || !req.body.username || !req.body.password)
        {
            res.status(400).json({err: "Parameter is null"});
        }
        else
        {
			// get lat-long using geolocation API
			let address =  (req.body.address).split(' ').join('+') + ',+' + (req.body.city).split(' ').join('+') + ',+Cyprus';
			const response = await fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + process.env.MAPS_API_KEY);
			const json = await response.json();
			
			const latitude = json.results[0].geometry.location.lat;
			const longitude = json.results[0].geometry.location.lng;

            // Query
            const results = await sqlQuery(`
            INSERT INTO SHOPS(Name, Address, Latitude, Longitude, City, PostalCode, PhoneNumber, Username, Password, CompanyID)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            `, [req.body.name, req.body.address, latitude, longitude, req.body.city, req.body.postalCode, req.body.phoneNumber, req.body.username, req.body.password, companyID]);

            res.status(200).json({success: true});
        }
     }

     // PUT: Update shop of this company
     else if(req.method == "PUT")
     {
         // Check if ShopID exists
         if(!req.body.shopID)
            res.status(400).json({err: "Parameter is null"});
        else
        {
			
			// get lat-long using geolocation API
			let latitude = null;
			let longitude = null;
			if(req.body.address) {
				let address =  (req.body.address).split(' ').join('+');
				if(req.body.city) {
					address = address + ',+' + (req.body.city).split(' ').join('+');
				}	
				address = address + ',+Cyprus';
				const response = await fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + process.env.MAPS_API_KEY);
				const json = await response.json();
                
				latitude = json.results[0].geometry.location.lat;
				longitude = json.results[0].geometry.location.lng;
			}

            // Query
            const result = await sqlQuery(`
            UPDATE SHOPS
            SET 
            Name = COALESCE(?, Name),
            Address = COALESCE(?, Address),
            Latitude = COALESCE(?, Latitude),
            Longitude = COALESCE(?, Longitude),
            City = COALESCE(?, City),
            PostalCode = COALESCE(?, PostalCode),
            PhoneNumber = COALESCE(?, PhoneNumber),
            Username = COALESCE(?, Username),
            Password = COALESCE(?, Password)
            WHERE ShopID = ? AND CompanyID = ?;
            `, [req.body.name, req.body.address, latitude, longitude, req.body.city, req.body.postalCode, req.body.phoneNumber, req.body.username, req.body.password, req.body.shopID, companyID]);

            res.status(200).json({success: true});
        }
     }

     else if(req.method == "DELETE")
     {
        // Check if ShopID exists
        if(!req.body.shopID)
            res.status(400).json({err: "Parameter is null"});

        // Delete stock of this shop
        const r1 = await sqlQuery(`
        DELETE FROM SELLS
        WHERE ShopID = ?;        
        `, [req.body.shopID, companyID]);

        // Delete shop
        const r2 = await sqlQuery(`
        DELETE FROM SHOPS
        WHERE ShopID = ?;
        `, [req.body.shopID, companyID]);

        res.status(200).json({success: true});;
     }
     else
    {   
        //console.log("Error: Unknown request (?)", [req.method]);
        res.status(500).json({err: "Unknown Request"});
    }
}

async function getCompanyID(username)
{
//console.log("Username: " +username);
    let temp = await sqlQuery(`
    SELECT CompanyID
    FROM COMPANIES
    WHERE Username = ?
    `, [username]);
//console.log("Query: "+ JSON.stringify(temp));
//console.log("cmopanyID = " + temp[0].CompanyID)

    if(!temp || Object.keys(temp).length <= 0)
        return null;
    
    return temp[0].CompanyID;
}
