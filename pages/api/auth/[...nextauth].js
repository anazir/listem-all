import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { sqlQuery } from '../../../lib/db';

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    ...['User', 'Shop', 'Company'].map(a => CredentialsProvider({
      id: a.toLowerCase(),
      name: a + ' Account',
      credentials: {
        username: { label: "Username", type: "text", placeholder: "jsmith" },
        password: {  label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        let temp = a;
        if(a === 'Company') temp = 'Companie';

        const users = await sqlQuery(`
          SELECT *
          FROM ${temp.toUpperCase()}S
          WHERE Username = ? AND Password = ?
        `, [ credentials.username, credentials.password ]);

        if (users[0]) {
          const { Username, Name, Surname, Email, CompanyID, ShopID } = users[0];

          return {
            username: Username,
            shopID: ShopID,
            companyID: CompanyID,
            name: Name,
            surname: Surname,
            email: Email,
            userType: a.toLowerCase()
          };
        }
        return null
      },
    }))
  ],
  session: {jwt: true,},
  callbacks: {
    jwt: async (token, user, account) => {
      user && (token.user = user);
      return token;
    },
    session: async (session, user, sessionToken) => {
      session.user = user.user;
      return session;
    },
    redirect(url, baseUrl) {
      if (url.startsWith(baseUrl)) return url;
      // Allows relative callback URLs
      else if (url.startsWith("/")) return new URL(url, baseUrl).toString();
      return baseUrl;
    }
  },
});
