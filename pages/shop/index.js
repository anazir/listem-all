import { useState, useEffect } from 'react';
import NavBar from '../../components/navbar';
import { useSession } from "next-auth/client";
import AuthRoute from "../../components/authRoute";

const ManageItems = () => {
	const [items, setItems] = useState([]);
	const [quantity, setQuantity] = useState('');
	const [searchValue, setSearchValue] = useState('');
	const [session, loading] = useSession();
	const [currentItem, setCurrentItem] = useState(null);
	const [trigger, setTrigger] = useState(0);

	const handleSubmit = async (itemID) => {
		await fetch('/api/shop-manager/items', {
			method: 'PUT',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({itemID, stock:quantity, shopID:session?.user?.shopID})
		});
		setTrigger(trigger + 1);
		setCurrentItem(null);
		setQuantity('');
	};	

	useEffect(() => {
		const foo = async () => {
			const response = await fetch("/api/shop-manager/items");

			const json = await response.json();
			setItems(json.items);
		};
		foo();
	}, [session, trigger]);

	return (
		<AuthRoute userType="shop">
			<NavBar />
			<div className="container is-centered mt-3">
				<h1 className="title"> Manage Items </h1>
				<div className="block">
					<input className="input" placeholder="Search Products" type="text" value={searchValue} onChange={(e) => setSearchValue(e.target.value) & setCurrentItem(null)}/>
				</div>
				<table className="table is-fullwidth">
					<tr className="thead">
						<th width="20%">Category</th>
						<th width="30%">Brand</th>
						<th width="40%">Description</th>
						<th width="5%">Price</th>
						<th width="5%">Quantity</th>
						<th width="5%"></th>
					</tr>
					{	
						items?.filter(a => (a.Category.toLowerCase().includes(searchValue.toLowerCase())
							|| a.Description.toLowerCase().includes(searchValue.toLowerCase())
							|| a.Brand.toLowerCase().includes(searchValue.toLowerCase())
						)).map((a, i) => (
							<tr>
								<td width="20%">{a.Category}</td>
								<td width="30%">{a.Brand}</td>
								<td width="40%">{a.Description}</td>
								<td width="5%">{a.Price}</td>
								{
									currentItem === i ?
										<>
											<td width="5%">
												<div class="control">
													<input className="input" type="text" size='2' value={quantity} onChange={(e) => setQuantity(e.target.value)}/>
												</div>
											</td>
											<td width="5%">
												<div class="control">
													<button className="button is-primary" onClick={() => handleSubmit(a.ItemID)}>Submit</button>
												</div>
											</td>
										</> :
										<>
											<td width="5%">{a.Quantity}</td>
											<td><button className="button is-info" onClick={() => setCurrentItem(i) & setQuantity(a.Quantity)}>Edit Stock</button></td>
										</>
								}
							</tr>
						))
					}
				</table>
			</div>
		</AuthRoute>
	)
};

export default ManageItems;
