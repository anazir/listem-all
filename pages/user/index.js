import { useState, useEffect } from 'react';
import useLocalStorageState from '../../components/useLocalStorage';
import AuthRoute from '../../components/authRoute';
import NavBar from '../../components/navbar';
import GoogleMapReact from 'google-map-react';

const markerStyle = {
  position: 'absolute',
  width: 30,
  height: 30,
  left: -30 / 2,
  top: -30 / 2,

  border: '5px solid #f44336',
  borderRadius: 30,
  backgroundColor: 'white',
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  padding: 4,
  cursor: 'pointer'
};

const Marker = ({ selected, text, ...props }) => {
  return (
    <div style={{...markerStyle, borderColor: selected ? '#3e8ed0' : '#f14668' }}>
      <div className="tag is-primary" style={{
        display: !props.$hover ? 'none' : null,
        marginTop: '-10rem'
      }}>
        {text}
      </div>
    </div>
  )
};

export default function ChooseCompany() {
  const [companies, setCompanies] = useState([]);
  const [shops, setShops] = useState([]);
  const [filteredShops, setFilteredShops] = useState([]);
  const [selectedShops, setSelectedShops] = useLocalStorageState('selectedShops', []);
  const [address, setAddress] = useState('');
  const [center, setCenter] = useState([35.15, 33.40]);

  // Fetch companies
  useEffect(() => {
    const foo = async () => {
      const response = await fetch('/api/simple-user/companies');
      const json = await response.json();

      // Add cheked attribute to each company
      setCompanies(json.companies?.map(a => ({ ...a, checked: true })));
    };

    // Call fetch function
    foo();
  }, []);

  // Fetch shops
  useEffect(() => {
    const foo = async () => {
      const response = await fetch('/api/simple-user/shops');
      const json = await response.json();

      setShops(json.shops);
      setFilteredShops(json.shops);
    };

    // Call fetch function
    foo();
  }, [companies?.length]);

  // Handler for change of checkboxes
  const handeChange = (checked, index) => {
    const temp = companies;
    temp[index].checked = checked;

    setCompanies(temp);
    setFilteredShops(shops.filter(a => temp.some(b => b.checked && b.CompanyID === a.CompanyID)));
  }

  const handleMarkClick = (key) => {
    const curShop = filteredShops[key];

    const tempShops = selectedShops.filter(a => a.ShopID !== curShop.ShopID);

    if(tempShops.length !== selectedShops.length) {
      setSelectedShops(tempShops);
    } else {
      setSelectedShops([ ...selectedShops, curShop ]);
    }
  };

  const handleAddressSelect = async () => {
    const response = await fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + process.env.NEXT_PUBLIC_MAPS_API_KEY);
    const json = await response.json();

    setCenter([json.results[0]?.geometry?.location?.lat, json.results[0]?.geometry?.location?.lng]);
  }

  return (
    <AuthRoute userType="user">
      <NavBar />

      <div className="container">
        <div className="columns">
          <div className="column is-4">
            <div className="field has-addons">
              <div className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  placeholder="Address"
                  value={address}
                  onChange={e => setAddress(e.target.value)}
                />
              </div>
              <div className="control">
                <a className="button is-primary" onClick={handleAddressSelect}>
                  Search
                </a>
              </div>
            </div>

            <article className="panel is-primary">
              <p className="panel-heading">
                Choose Companies
              </p>

              {
                // Display each company's name with a checkbox
                companies?.map((a, i) => (
                  <div className="panel-block">
                    <label className="checkbox">
                      <input checked={a.checked} type="checkbox" onChange={e => handeChange(e.target.checked, i)} />
                      {a.Name}
                    </label>
                  </div>
                ))
              }
            </article>

            <a href={"/user/search-products"} className="button is-info">Next</a>
          </div>

          <div className="column hero is-fullheight-with-navbar">
            <div className="box" style={{height: '100%'}}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: process.env.NEXT_PUBLIC_MAPS_API_KEY }}
                defaultCenter={[35.15, 33.40]}
                center={ center }
                defaultZoom={14}
                onChildClick={handleMarkClick}
              >
                {
                  filteredShops?.map(a =>
                    <Marker
                      selected={selectedShops.find(b => b.ShopID === a.ShopID)}
                      text={a.Name}
                      lat={a.Latitude}
                      lng={a.Longitude}
                    />
                  )
                }
              </GoogleMapReact>
            </div>
          </div>
        </div>
      </div>
    </AuthRoute>
  )
}
