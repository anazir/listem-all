import { useState, useEffect } from 'react';
import AuthRoute from '../../components/authRoute';
import NavBar from '../../components/navbar';
import useLocalStorageState from '../../components/useLocalStorage';

export default function SearchItems() {
  const [category, setCategory] = useState('');
  const [items, setItems] = useState([]);
  const [added, setAdded] = useLocalStorageState('added', []);
  const [trigger, setTrigger] = useState(0);
  const [shops, setShops] = useLocalStorageState('selectedShops', []);
  const [selectedItems, setSelectedItems] = useLocalStorageState('selectedItems', []);

  const handleSearch = async () => {
    // Search for impossible category if nothing in category
    const url = `/api/simple-user/items?category=${category}&shops=${shops?.map(a => a.ShopID)}`;
    const response = await fetch(url);
    const json = await response.json();

    setItems(json.items);

    items?.sort((a,b) => a.shopID > b.shopID); // sort items by shop id to match shops
    items?.forEach(s => s.products.sort((a,b) => a.Brand > b.Brand)); // sort items by brand to match rows
  };

  const handleAdd = async (shopIndex, itemIndex) => {
    const curProduct = items[shopIndex].products[itemIndex];
    let temp = selectedItems;

    temp[shopIndex].items.push({
      name: curProduct.Brand,
      price: curProduct.Price
    });

    console.log(temp)
    setSelectedItems(temp);

    temp = added;
    temp.push(shops[shopIndex].ShopID + curProduct.Brand);
    setAdded(temp);
    setTrigger(trigger+1);
  }

  useEffect(() => { handleSearch(); }, [trigger]); 

  useEffect(() => {
    handleSearch();
    if (shops) {
      const temp = [...shops];
      temp?.sort((a,b) => a.ShopID.toString() > b.ShopID.toString());

      const newSelected = temp?.map(a => ({
        name: a.Name,
        shopID: a.ShopID,
        // Get items already in local storage oherwise get an empty array
        items: selectedItems?.reduce((pr2, cur) => cur.shopID === a.ShopID ? cur.items : pr2, []) || []
      })) || selectedItems;

      setSelectedItems(newSelected);
    }
    console.log(shops, selectedItems, window.localStorage.getItem('selectedItems'))
  }, [shops]);

  return (
    <AuthRoute userType="user">
      <NavBar />

      <div className="container">
        <div className="columns is-multiline">
          <div className="column is-6">
            <h2 className="title is-2">Search Items</h2>
            <p className="subtitle is-6">Search for desired items by category and add them to your shopping lists</p>
          </div>
          <div className="column is-6">
            <div className="try-demo">
              {<a className="button is-primary is-medium is-pulled-right" href="/user/lists">View Lists</a>}
            </div>
          </div>
        </div>
        <div className="field has-addons">
          <div className="control is-expanded">
            <input
              className="input"
              type="text"
              placeholder="Search Category"
              value={category}
              onChange={e => setCategory(e.target.value)}
            />
          </div>
          <div className="control">
            <a className="button is-primary" onClick={handleSearch}>
              Search
            </a>
          </div>
        </div>
        <div className="columns is-multiline">
          {
            shops?.map((s, i) => (
              <div className="column is-3" key={i}>
                <h3 className="title is-4">{s.Name}</h3>
                <table className="table">
                  <tbody>
                    {
                      (items || [])[i]?.products.map((a, j) => (
                        <tr>
                          <td>{a.Brand}</td>
                          <td>{a.Price}</td>

                          {
                            !added.includes(shops[i].ShopID + items[i].products[j].Brand) ? 
                              <td>
                                <button className="button is-info" onClick={_ => handleAdd(i, j)}>+</button>
                              </td> :
                              <td className="has-text-success"><i>Added to list</i></td>
                          }
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
              </div>
            ))
          }
        </div>	
      </div>
    </AuthRoute>
  )
}
