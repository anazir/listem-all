import useLocalStorageState from '../../components/useLocalStorage';
import AuthRoute from '../../components/authRoute';
import NavBar from '../../components/navbar';

export default function ListInterface() {
  const [items, setItems] = useLocalStorageState('selectedItems', []);
  const [added, setAdded] = useLocalStorageState('added', []);

  const handleDelete = (shopIndex, itemIndex) => {
    const curShop = items[shopIndex];
    const curItem = curShop.items[itemIndex];
    setAdded(added.filter(a => a !== curShop.shopID + curItem.name));

    const fixShop = (shop) => {
      const temp = shop;
      temp.items = temp.items.filter((a, i) => i !== itemIndex);

      return shop;
    };

    const temp = items;
    temp = temp
      .map((a, i) => shopIndex === i ? fixShop(a) : a)
      .filter(a => a.items.length > 0);

    setItems(temp);
  }

  const renderTableRows = (shop, i) => {
    return shop.items.map((item, j) => {
      return (
        <tr key={item.name}>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td><button className="button is-danger" onClick={_ => handleDelete(i, j)}>-</button></td>
        </tr>
      )
    })
  }

  return (
    <AuthRoute userType="user">
      <NavBar />

      <div className="container">
        <h1 className="title is-1">Lists</h1>
        <h2 className="subtitle">View and delete items from your created lists</h2>
        <div className="columns is-multiline">
          {
            items?.map((shop, i) => (
              <div className="column" key={i}>
                <h2 className="title is-3">{shop.name}</h2>
                <table className="table">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Price</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {renderTableRows(shop, i)}
                    <tr>
                      <td><strong>Total</strong></td>
                      <td><strong>{shop.items.reduce((pr, cur) => (cur.price + pr), 0).toFixed(2)}</strong></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            ))
          }
        </div>
      </div>
    </AuthRoute>
  );
}
