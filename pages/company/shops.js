import { useState, useEffect } from 'react';
import NavBar from '../../components/navbar';
import { useSession } from "next-auth/client";
import AuthRoute from "../../components/authRoute";

const ManageCompanyShops = () => {
  const [shops, setShops] = useState([]);
  const [name, setName] = useState ('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [postalCode, setPostalCode] = useState(0);
  const [phoneNumber, setPhoneNumber] = useState(0);
  const [selectedShop, setSelectedShop] = useState(null);
  const [selectedDelete, setSelectedDelete] = useState(null);
  const [searchValue, setSearchValue] = useState('');


  const [session, loading] = useSession();
  const [trigger, setTrigger] = useState(0);

  const handleSubmit = async (shopID) => 
  {
    await fetch("/api/company-manager/shops",
      {
        method: "PUT",
        headers:
        {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({shopID, name, address, city, postalCode, phoneNumber})
      });

    setTrigger(trigger + 1);
    setSelectedShop(null);
  };

  const handleDelete = async (shopID) =>
  {
    await fetch("/api/company-manager/shops",
      {
        method: "DELETE",
        headers:
        {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({shopID})
      });

    setTrigger(trigger + 1);
    setSelectedDelete(null);
  }

  useEffect(() => 
    {
      const foo = async () => 
      {
        const response = await fetch("/api/company-manager/shops");

        const json = await response.json();
        setShops(json.shops);
      };
      foo();
    }, [session, trigger]);

  return (
    <AuthRoute userType="company">
      <NavBar />
      <div className="container is-centered mt-3">
        <h1 className="title"> Manage Shops </h1>
        <div>
          <input className="input" placeholder="Search Shops" type="text" value={searchValue} onChange={(e) => setSearchValue(e.target.value) 
              & setSelectedShop(null) & setSelectedDelete(null)}/>
        </div>
        <table className="table is-fullwidth">
          <tr className="thead">
            <th>Name</th>
            <th>Address</th>
            <th>City</th>
            <th>PostalCode</th>
            <th>PhoneNumber</th>
            <th></th>
            <th></th>
          </tr>
          {
            shops?.filter(a => (a.Name.toLowerCase().includes(searchValue.toLowerCase())
              || a.Address.toLowerCase().includes(searchValue.toLowerCase())
              || a.City.toLowerCase().includes(searchValue.toLowerCase())
              || a.PostalCode.toString().includes(searchValue)
              || a.PhoneNumber.toString().includes(searchValue)

            )).map((a,i) => (
              <tr>

                {
                  selectedShop === i ?
                    <>
                      <td>
                        <div>
                          <input className="input" placeholder={a.Name} type="text" /*size ='16' */value={name} onChange={(e) => setName(e.target.value)}/>
                        </div>
                      </td>

                      <td>
                        <div>
                          <input className="input" placeholder={a.Address} type="text" /*size ='32' */value={address} onChange={(e) => setAddress(e.target.value)}/>
                        </div>
                      </td>
                      <td>
                        <div>
                          <input className="input" placeholder={a.City} type="text" /*size ='32' */value={city} onChange={(e) => setCity(e.target.value)}/>
                        </div>
                      </td>
                      <td>
                        <div>
                          <input className="input" placeholder={a.PostalCode} type="number" /*size ='4' */value={postalCode} onChange={(e) => setPostalCode(e.target.value)}/>
                        </div>
                      </td>
                      <td>
                        <div>
                          <input className="input" placeholder={a.PhoneNumber} type="number" /*size ='8' */value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)}/>
                        </div>
                      </td>
                      <td><button className="button is-primary" onClick={() => handleSubmit(a.ShopID)}>Submit</button></td>
                    </> :

                    <>
                      <td width="17%">{a.Name}</td>
                      <td width="25%">{a.Address}</td>
                      <td width="10%">{a.City}</td>
                      <td width="10%">{a.PostalCode}</td>
                      <td width="10%">{a.PhoneNumber}</td>
                      <td width="10%"><button className="button" onClick= {() => setSelectedShop(i) & setSelectedDelete(null)
                          & setName(a.Name) & setAddress(a.Address) & setCity(a.City) & setPostalCode(a.PostalCode) & setPhoneNumber(a.PhoneNumber)}>Edit</button></td>
                    </>
                }

                {
                  selectedDelete === i ?
                    <td width="18%"><button className="button is-danger" onClick= {() => handleDelete(a.ShopID)}>Confirm Delete?</button></td>:
                    <td width="18%"><button className="button is-danger" onClick= {() => setSelectedDelete(i) & setSelectedShop(null)}>Delete</button></td>

                }
              </tr>
            ))
          }
        </table>
      </div>
    </AuthRoute>
  )
};

export default ManageCompanyShops;
