import { useState, useEffect } from 'react';
import NavBar from '../../components/navbar';
import { useSession } from "next-auth/client";
import AuthRoute from "../../components/authRoute";

const ManageCompanyItems = () => {
  const [items, setItems] = useState([]);
  const [description, setDescription] = useState ('');
  const [brand, setBrand] = useState('');
  const [category, setCategory] = useState('');
  const [price, setPrice] = useState(0);
  const [selectedItem, setSelectedItem] = useState(null);
  const [selectedDelete, setSelectedDelete] = useState(null);
  const [searchValue, setSearchValue] = useState('');


  const [session, loading] = useSession();
  const [trigger, setTrigger] = useState(0);
  const cid = session?.user.companyID;

  const handleSubmit = async (itemID) => 
  {
    await fetch("/api/company-manager/items",
      {
        method: "PUT",
        headers:
        {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({itemID, description, brand, category, price, cid})
      });

    setTrigger(trigger + 1);
    setSelectedItem(null);
  };

  const handleDelete = async (itemID) =>
  {
    await fetch("/api/company-manager/items",
      {
        method: "DELETE",
        headers:
        {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({itemID})
      });

    setTrigger(trigger + 1);
    setSelectedDelete(null);
  }
  useEffect(() => 
    {
      const foo = async () => 
      {
        const response = await fetch("/api/company-manager/items");

        const json = await response.json();
        setItems(json.items);
      };
      foo();
    }, [session, trigger]);

  return (
    <AuthRoute userType="company">
      <NavBar />
      <div className="container is-centered mt-3">
        <h1 className="title"> Manage Items </h1>
        <div>
          <input className="input" placeholder="Search Items" type="text" value={searchValue} onChange={(e) => setSearchValue(e.target.value) 
              & setSelectedItem(null) & setSelectedDelete(null)}/>
        </div>
        <table className="table is-fullwidth">
          <tr className="thead">
            <th width="20%">Category</th>
            <th width="30%">Brand</th>
            <th width="40%">Description</th>
            <th width="15%">Price</th>
            <th width="5%"></th>
          </tr>
          {
            items?.filter(a => (a.Category.toLowerCase().includes(searchValue.toLowerCase())
              || a.Description.toLowerCase().includes(searchValue.toLowerCase())
              || a.Brand.toLowerCase().includes(searchValue.toLowerCase())
              || a.Price.toString().toLowerCase().includes(searchValue.toLowerCase())

            )).map((a,i) => (
              <tr>

                {
                  selectedItem === i ?
                    <>
                      <td>
                        <div>
                          <input className="input" placeholder={a.Category} type="text" /*size ='16' */value={category} onChange={(e) => setCategory(e.target.value)}/>
                        </div>
                      </td>

                      <td>
                        <div>
                          <input className="input" placeholder={a.Brand} type="text" /*size ='32' */value={brand} onChange={(e) => setBrand(e.target.value)}/>
                        </div>
                      </td>
                      <td>
                        <div>
                          <input className="input" placeholder={a.Description} type="text" /*size ='32' */value={description} onChange={(e) => setDescription(e.target.value)}/>
                        </div>
                      </td>
                      <td>
                        <div>
                          <input className="input" placeholder={a.Price} type="number" /*size ='4' */value={price} onChange={(e) => setPrice(e.target.value)}/>
                        </div>
                      </td>

                      <td><button className="button" onClick={() => handleSubmit(a.ItemID)}>Submit</button></td>
                    </> :

                    <>
                      <td width="20%">{a.Category}</td>
                      <td width="30%">{a.Brand}</td>
                      <td width="40%">{a.Description}</td>
                      <td width="5%">{a.Price}</td>
                      <td width="10%"><button className="button" onClick= {() => setSelectedItem(i) & setSelectedDelete(null)
                          & setCategory(a.Category) & setBrand(a.Brand) & setDescription(a.Description) & setPrice(a.Price)}>Edit</button></td>
                    </>
                }

                {
                  selectedDelete === i ?
                    <td width="18%"><button className="button is-danger" onClick= {() => handleDelete(a.ItemID)}>Confirm Delete?</button></td>:
                    <td width="18%"><button className="button is-danger" onClick= {() => setSelectedDelete(i) & setSelectedItem(null)}>Delete</button></td>

                }
              </tr>
            ))
          }
        </table>
      </div>
    </AuthRoute>
  )
};

export default ManageCompanyItems;
