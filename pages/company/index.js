import { useState, useEffect } from 'react';
import NavBar from '../../components/navbar';
import { useSession } from "next-auth/client";
import AuthRoute from "../../components/authRoute";

const CompanyManagerHome = () => {
	const [session, loading] = useSession();

	return (
		<AuthRoute userType="company">
			<NavBar />
			<div>
			  <section className="hero is-primary is-bold">
				<div className="hero-body splash">
				  <div className="container">
					<div className="columns is-vcentered">
					  <div className="column">
						<h1 className="title is-spaced is-size-2-desktop is-size-1-fullhd">Manage your resources</h1>
					  </div>
					</div>
				  </div>
				</div>
			  </section>

			  <section className="section" id="how-it-works">
				<div className="container has-text-centered">

				  <div className="columns step is-vcentered">
					<div className="column is-6">
					  <div className="how-it-works-preview box-shadow has-white-bg">
						<img src="/images/shop_management_icon.jpg" alt="Shop Management Icon" />
					  </div>
					</div>
					
					<div className="column is-6">
					  <div className="how-it-works-preview box-shadow has-white-bg">
						<img src="/images/item_management_icon.jpg" alt="Item Management Icon" />
					  </div>
					</div>
				  </div>

				  <div className="columns step is-vcentered">
					<div className="column is-6">
					  <h3 className="title is-size-4-mobile">Manage your shops</h3>
					  <p>Here you can view, add, edit and delete your shops</p>
					</div>
					
					<div className="column is-6">
					  <h3 className="title is-size-4-mobile">Manage your items</h3>
					  <p>Here you can view, add, edit and delete your items</p>
					</div>
					
				  </div>
				  
				  <div className="columns step is-vcentered">
					<div className="column is-6">
						<div className="try-demo">
							{<a className="button is-primary is-medium" href="/company/shops">Manage Shops</a>}
						</div>
					</div>
					
					<div className="column is-6">
						<div className="try-demo">
							{<a className="button is-primary is-medium" href="/company/items">Manage Items</a>}
						</div>
					</div>
					
				  </div>
				</div>
			  </section>
			</div>
		</AuthRoute>
	)
};

export default CompanyManagerHome;
