import { useEffect } from "react";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";

export default function AuthRoute({ userType, children }) {
  const [session, loading] = useSession()
  const router = useRouter();

  useEffect(() => {
    if(!loading) {
      if (!session) {
        router.push('/');
      } else if (session.user.userType !== userType) {
        router.push('/' + session.user.userType);
      }
    }
  }, [loading, session, userType]);

  // Only display children if user is authenticated
  return !loading && session && session.user.userType === userType && children;
};
