import { signOut } from "next-auth/client";

export default function NavBar() {
  return (
    <nav className="navbar has-shadow"  role="navigation" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item has-text-weight-bold" href="/">
            List'em All
          </a>
        </div>

        <div id="bulma-navbar-menu" className="navbar-menu">
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a onClick={signOut} className="button is-primary">Log out</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}
