import { useState, useEffect } from 'react';

export default function useLocalStorageState(key, defaultValue = "") {
  const [state, setState] = useState(null);
  // In case that state is an object, changes won't cause a rerender. This
  // fixes it
  const [updateTrigger, setUpdateTrigger] = useState(0);

  useEffect(() => {
    setState(JSON.parse(window.localStorage.getItem(key)) || defaultValue);
  }, []);

  const setLocalStorageState = (newState) => {
    setState(newState);
    setUpdateTrigger(pr => pr + 1);
    window.localStorage.setItem(key, JSON.stringify(newState));
  }

  return [state, setLocalStorageState];
}
