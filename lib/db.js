import mysql from 'serverless-mysql';

export const db = mysql({
  config: {
    host: process.env.MYSQL_HOST,
    database: process.env.MYSQL_DATABASE,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
  }
});

export const sqlQuery = async (query, values = []) => {
  try {
    const results = await db.query(query, values);
    await db.end();
    return results;
  } catch(err) {
    throw Error(err);
  }
};
